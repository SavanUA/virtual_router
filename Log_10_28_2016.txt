1. 'make world' does -
    a. make clean
    b. make dist (compile)
    c. use sbuild & schroot to compile in a sandboxed env with relevant ARM32 tool-chain
    d. configure for ARM32 arch.
    
        CONFIG_SITE=/etc/dpkg-cross/cross-config.armhf ./configure --build=x86_64-unknown-linux-gnu --host=arm-linux-gnueabihf
        
        make dist-tools CROSS_COMPILE=arm-linux-gnueabihf- XEN_TARGET_ARCH=arm32
        
2. Output installed locally in folder dist/
3. Need to invoke 'make install' to actually install to host machine
    a. Or copy dist/ folder to remote machine and run ./install script
4. Before installing xen on router -
    a. check if we require a different tool-chain
    b. install grub on router
    c. prepare a domU (a flavor of openWRT)
