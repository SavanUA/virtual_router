1. Can't push Xen onto Netgear router directly as the files system is mounted as read-only.
   a. Need to download DD-WRT source (or) Firmware modification kit and push Xen onto it and re-flash on router.
   b. Maybe, in the process, add a device-tree and look at the boot-loader source and configuration.
2. Looks like sd-card on Raspberry Pi is corrupted. Confirm with Raspbian image once.
   a. Images are getting flashed successfully. However nothing is ever booting up.
   b. Are we missing something here ????
3. UART successfully established on both Router and Raspberry Pi
   a. Made debugging and connecting to device a lot easier.
4. 
